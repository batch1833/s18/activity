//console.log("Hello world")


//Create a trainer object using object literals
let trainer = {

//4. Initialize/add the following trainer object properties:
	name: "Ash Ketshum",
	age: 10,
	pokemon:["pikachu","charizard", "Squirtle", "Bulbasaur"],
	friends:{
	 	hoenn: ["May","Max"],
	 	kanto: ["Brock","Misty"]
	 	},
//5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
		talk: function(){
	 		console.log(this.pokemon[0] + " I choose you!")
	 		
	 	}
}

console.log(trainer);


//6. Access the trainer object properties using dot and square bracket notation.
console.log("Result of square notation: " );
console.log(trainer.name);
console.log("Result of bracket notation: " );
console.log(trainer["pokemon"]);

//7. Invoke/call the trainer talk object method.
console.log("Result of talk method: " );
trainer.talk();

//8. Create a constructor for creating a pokemon with the following properties:

function Pokemon(name,level){
	//Properties
	this.name = name;
	this.level = level;
	this.health = level *2; 
	this.attack = level;	

//10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.	
	this.tackle = function(target){ 
			console.log(this.name+" tackled "+ target.name);

			target.health -= this.attack;
			if(target.health <= 0){
				target.faint();
			}
			else{
				console.log(target.name + "'s health is now reduced to " + target.health);
			}
}
this.faint = function(){
	console.log(this.name + " fainted ");
	}
}
//9. Create/instantiate several pokemon objects from the constructor with varying name and level properties.
let pikachu = new Pokemon("Pikachu", 16);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 20);
console.log(geodude);

let Mewtwo = new Pokemon("Mewtwo", 30);
console.log(Mewtwo);

geodude.tackle(pikachu);


			
